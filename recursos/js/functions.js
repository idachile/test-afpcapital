// Data picker configuration


$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '<',
    nextText: '>',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
   $(function () {
   $("#fecha").datepicker();
   });
   // Data picker configuration

   // Responsive Table 
   
   $(document).ready(function(){
      
    $('.cpl_realTable td').prepend('<div class="th_movil">h</div>');
    // $('.cpl_realTable thead').prepend('<h2>Tus solicitudes</h2>');
  
    $(".clickable-row").click(function() {
      window.location = $(this).data("href");
  });
  var table = document.getElementsByClassName('cpl_realTable'); 
  
  for(i=0; i < table.length; i++){
  
   var tableHead = table[i].getElementsByTagName('thead');
  
  
   var indexVal = tableHead[0].getElementsByClassName('th-sm');
    
    var fila = table[i].getElementsByTagName('tr');
    
    for(f=0;f<fila.length;f++){
      
      var columnas = fila[f].getElementsByClassName('th_movil');
      
      for(c=0;c<columnas.length;c++){
        
        columnas[c].innerHTML = indexVal[c].innerHTML;
      }
  
    }
  }

});
  // Responsive Table 

  //modales//

function BuenosModales(modalClass){
    // $('html').css('overflow', 'hidden');
      $('html').css({overflow: 'hidden', width: '100%'});
 $('.'+modalClass).fadeIn(100);
 $('.'+modalClass).find('.modal_box').show();
 $('.'+modalClass).find('.modal_box').addClass('animated');
 $('.'+modalClass).find('.top').addClass('animated');
if($('.'+modalClass).hasClass('right_modal')){
  $('.'+modalClass).find('.modal_box').addClass('fadeInRight');
  $('.'+modalClass).find('.top').addClass('fadeInRight');
}
if($('.'+modalClass).hasClass('center_modal')){
  // $('.'+modalClass).addClass('cpl_center-content');
  $('.'+modalClass).find('.modal_box').addClass('animated fadeInDown');
}
 $('.'+modalClass).find('.close_action').click(function(){
    cerrarModal(modalClass);
 });

//  $('.'+modalClass).click(function() {
//     cerrarModal(modalClass);
// });
// $('.'+modalClass+' .modal_box').click(function (e) {
//     e.stopPropagation();
// });

 function cerrarModal(modalClass){
    
    $('.'+modalClass).fadeOut(0);
  
    $('.'+modalClass).find('.modal_box').delay(1000).removeClass('animated fadeInUp fadeInRight fadeInDown fadeOutRight ');
    $('.'+modalClass).find('.top').delay(1000).removeClass('animated fadeInUp fadeInRight fadeInDown fadeOutRight ');
    if($('.'+modalClass).hasClass('center_modal')){
      // $('.'+modalClass).removeClass('cpl_center-content');
   
    }
    // $('html').css('overflow', 'auto');
    $('html').css({overflow: 'auto', width: '100%'});
    
 }


}

  //modales//